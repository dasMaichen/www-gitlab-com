---
layout: markdown_page
title: "Meltano (BizOps Product)"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary

[Meltano](https://gitlab.com/meltano/meltano) is a convention-over-configuration framework for analytics, business intelligence, and data science. It leverages open source software and software development best practices including version control, CI, CD, and review apps.

## Team

* [Sid](https://gitlab.com/sytses)
* [Jacob](https://gitlab.com/jschatz1)
* [Micaël](https://gitlab.com/mbergeron)
* [Yannis](https://gitlab.com/iroussos)
* [Alex](https://gitlab.com/zamai)

## Code

See the active project here: [gitlab.com/meltano/meltano](https://gitlab.com/meltano/meltano)
