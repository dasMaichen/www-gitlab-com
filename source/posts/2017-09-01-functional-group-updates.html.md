---
title: "GitLab's Functional Group Updates: August 22nd - August 30th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/functional-group-updates/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Edge Team

[Presentation slides](http://gitlab-org.gitlab.io/functional-group-updates/edge/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/0GtEFa6LB8Y" frameborder="0" allowfullscreen="true"></iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Research Team

[Presentation slides](https://docs.google.com/a/gitlab.com/presentation/d/1NkLeFxwMldMbBu24L9RRpJOa-NXHtogM627dm4eZwR0)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/587tFIXTee0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Discussion Team

[Presentation slides](http://gitlab-org.gitlab.io/functional-group-updates/backend-discussion/2017-08-28/#1)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/UnEghMF9zKc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Product Team

[Presentation slides](https://www.slideshare.net/JobvanderVoort/product-update-aug29)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/dlAVrl90c4U" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### People Ops Team

[Presentation slides](https://docs.google.com/presentation/d/1j0Q6obviN5uMbNeXY3VTziXQG4PqaGiibtTglaO0Zhw/edit?ts=59a53a7f#slide=id.g156008d958_0_18)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/0b-kkr92zVM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
