---
layout: job_page
title: "Recruiter"
---

## Recruiter (Intermediate)

GitLab is looking for a recruiter to create positive experiences for GitLab candidates and hiring teams. We're growing quickly and need a dynamic team member to help us identify amazing candidates, improve our existing hiring practices, and deliver exceptional customer service. GitLab strives to be a preferred employer and will rely on the recruiters to act as brand ambassadors by embodying the company values and identifying those values in potential team members.  We need a recruiter who is enthusiastic about working with high volume and is dedicated to helping us build a qualified, diverse, and motivated team.

### Responsibilities

* Collaborate with managers to understand requirements and establish effective recruiting strategies
* Develop and advertise accurate job descriptions to attract a highly qualified candidate pool
* Identify creative and strategic ways to source great people
* Apply effective recruiting practices to passive and active candidates
* Source, Screen, interview and evaluate candidates
* Assess candidate interest and ability to thrive in an open source culture
* Foster lasting relationships with candidates
* Share best practice interviewing techniques with managers
* Build an effective network of internal and external resources to call on as needed
* Ensure candidates receive timely, thoughtful and engaging messaging throughout the hiring process
* Partner with Marketing to develop and deliver a disruptive employer brand strategy
* Promote our values, culture and remote only passion
* Distribute thoughtful and engaging employer brand content
* Design and monitor key metrics to evaluate the effectiveness of our employment practices
* Develop recommendations for course corrections by leveraging data from our ATS, post interview and post hire surveys and other feedback loops
* Continually search for opportunities to elevate our brand by identifying industry best practices, evaluating competitors and nurturing networks and partnerships

### Requirements

* Experience recruiting at all levels, preferably in a global capacity within the software industry, open source experience is a plus
* Proven success in recruiting and filling technical positions
* Demonstrated ability to effectively source and place candidates for all positions at all levels
* Experience with competitive global job markets preferred
* Focused on delivering an excellent candidate experience
* Ambitious, efficient and stable under tight deadlines and competing priorities
* Remote working experience in a technology startup will be an added advantage
* Ability to build relationships with managers and colleagues across multiple disciplines and timezones
* Working knowledge using an candidate tracking systems
* Outstanding written and verbal communication skills across all levels
* Willingness to learn and use software tools including Git and GitLab
* College / University degree in Marketing, Human Resources or related field from an accredited institution preferred
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).

### Levels

#### Intern

##### Responsibilities

* Understand the roles we need filled and the requirements for those roles.
* Develop candidate reports for key roles, outlining candidates that we should be targeting for our roles.
* Act as point-of-contact for candidate inquiries regarding the application process, available positions and other recruitment-related inquiries.
* Assist recruiters with sourcing/researching activities, utilizing a variety of resources, including Search Engines and Social Networking Media, to proactively and continually source, develop and maintain an effective pipeline.
* Assist in maintaining our internal database, continuously updating candidate records.
* Perform additional tasks, projects and responsibilities as assigned. This will include assisting GitLab in building an internship program with a strong diversity focus.

##### About you

* Minimum one (1) year of work experience, demonstrating responsibility and reliability (experience supporting recruiting staff preferred).
* Enrolled at an accredited University with a formal internship/co-op program.
* Comfortable using technology
* Experience working with Google Suite preferred.
* Ability to interpret substantial amounts of data.
* Excellent written & verbal communication skills both in person and over the phone/computer.
* Strong organizational skills, ability to take on multiple tasks and function under stressful situations and deadlines.
* Strong attention to detail and ability to maintain detail focus in a fast-paced environment.

#### Junior

Junior recruiters share the same requirements as the Intermexiate recruiter listed above, but typically join with less or alternate experience in one of the key areas of expertise (global experience, candidate tracking systems, technical recruiting experience, etc). Junior recruiters will be expected to:

1. Managing inbound candidate traffic for all assigned roles
1. Ensure positive candidate experiences for every candidate
1. Provide clear, thorough, and timely communications/feedback to candidates and hiring teams.
1. Identify top-tier candidates through resumes, cover letters, and initial calls.
1. Meet with hiring managers to collect job requirements and expectations
1. Write effective job descriptions

#### Senior

Senior recruiters share the same requirements as the Intermexiate recruiter listed above, but also carry the following: 

1. 5 plus yaers recruiting experience 
1. Experience sourcing and recruiting for senior level roles 
1. An aptitude for sharing best practies and becoming a subject matter expert in the role 
1. Proven ability to be a talent advisor to your hiring managers 
1. Lead by example and performance in the following areas: # of hires, Time to fill, Hiring Manager, and Candidate Satisfaction
1. Provide insight and feedback for process improvement opportunities 

####  Lead

### Responsibilities

- Own a req load with a primary focus on senior level/executive roles
- Collaborate with managers to understand requirements and establish effective recruiting strategies
- Identify creative and strategic ways to source great people
- Assess candidate interest and ability to thrive in an open source culture
- You will lead a collaborative remote based recruiting team that can scale to the dynamic demands of a rapidly growing world-wide technology company
- Provide an exceptional and high touch candidate experience
- Mentor, guide, and grow the careers of all team members
- Help define consistent data-driven hiring metrics and goals
- Create and execute innovative sourcing strategies and recruiting campaigns
- Act as a key business partner to members of the organization to improve processes for recruiting professionals
- Work closely with various internal functional groups to understand business requirements, and consult on talent solutions
- Stay connected to the competitive landscape, including trends in recruiting and compensation
- Ensure a that you and your team maintain a high level of data integrity with our ATS and other People systems

### Requirements
- You have at least 6 years of recruiting experience within a growing organization
- You have at least 2 years experience recruiting for executive level roles
- Consistent track record with sourcing, recruiting, and closing extraordinary talent (especially passive)
- You have worked closely with sourcers, recruiters, and coordinators
- Experience working directly with hiring managers
- High sense of urgency
- Proven organizational skills with high attention to detail and the ability to prioritize
- Confidence to learn new technologies (MAC, Google Suite, GitLab) and translate that learning to others
- Willingness to learn and use software tools including Git and GitLab
- Experience building and defining recruiting pipeline metrics and data
- Experience and proficiency with Applicant Tracking Systems and other recruiting software (ideally including Greenhouse and LinkedIn Recruiter)
- Successful completion of a [background check](https://gitlab.com/gitlab-com/www-gitlab-com/master/handbook/people-operations/code-of-conduct#background-checks)
- You share our values, and work in accordance with those [values](https://gitlab.com/gitlab-com/www-gitlab-com/master/handbook/values)

## Specialities

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

## Sales & Marketing

### Responsibilities

- Own a req load specific to Sales & Marketing
- Act as business partner for Sales & Marketing leaders to improve their teams and recruiting processes

### Requirements

- Experience recruiting for executive level roles and all other roles within Sales & Marketing
- Experience partnering with leaders within Sales & Marketing

## Engineering

### Responsibilities

- Own a req load specific to Engineering
- Act as business partner for Engineering leaders to improve their teams and recruiting processes

### Requirements

- Experience recruiting for executive level roles and all other roles within Engineering
- Experience partnering with leaders within Engineering

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters
- Next, candidates willbe invited to schedule a 30 minute interview with one of our Senior Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Recruiting Director
- After that, candidates will be invited to schedule a 30 minute interview with our Chief Culture Officer
- Next, the candidate will be invited to interview with a Leader on the Sales & Marketing team
- Finally, our CEO may choose to conduct a final interview

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).
